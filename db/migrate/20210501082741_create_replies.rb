class CreateReplies < ActiveRecord::Migration[5.2]
  def change
    create_table :replies, id: :uuid do |t|
      t.text :content
      t.uuid :post_id
      t.uuid :user_id

      t.timestamps
    end
  end
end
