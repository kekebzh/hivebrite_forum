# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
DatabaseCleaner.clean_with(:truncation)

Post.__elasticsearch__.create_index!(force: true)

10.times do
  Channel.create!(
      title: Faker::Movie.title
  )
end

20.times do
  User.create!(
    id: SecureRandom.uuid,
    firstname: Faker::Name.first_name,
    lastname: Faker::Name.last_name,
    email: Faker::Internet.email,
    password: "password",
    password_confirmation: "password",
  )
end

200.times do
  Post.create!(
    user_id: User.all.ids.sample,
    title: Faker::Lorem.sentence,
    content: Faker::Lorem.paragraph_by_chars(number: 500, supplemental: true),
    channel_id: Channel.all.ids.sample
  )
end

200.times do
  Reply.create!(
    content: Faker::Lorem.paragraph_by_chars(number: 500, supplemental: true),
    post_id: Post.all.ids.sample,
    user_id: User.all.ids.sample,
  )
end

