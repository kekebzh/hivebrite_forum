FROM ruby:2.7.3

RUN apt-get update -qq && apt-get install -y yarn nodejs postgresql-client

WORKDIR /hivebrite_forum
COPY . /hivebrite_forum
RUN bundle install

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]