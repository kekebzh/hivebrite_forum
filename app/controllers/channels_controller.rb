class ChannelsController < ApplicationController
  before_action :set_channel, only: [:show, :index]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @channels = Channel.all.order('created_at desc')
    @posts = Post.all.preload(:user, :channel).order('created_at desc')
  end

  def show
    @channels = Channel.all.order('created_at desc')
    @posts = Post.preload(:user, :channel).where('channel_id = ?', @channel.id).page(params[:page])
  end

  def new
    @channel = Channel.new
  end

  def edit
  end

  def create
    @channel = Channel.new(channel_params)
    if @channel.save
      redirect_to root_path, notice: "Channel successfully created."
    else
      render :new
    end
  end

  def update
    if @channel.update(channel_params)
      redirect_to root_path, notice: "Channel successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @channel.destroy
    redirect_to root_path, notice: "Channel successfully destroyed."
  end

  private

  def set_channel
    @channel = Channel.find(params[:id]) if params.key?(:id)
  end

  def permitted_params
    [
      :title,
    ]
  end

  def channel_params
    params.fetch(:channel, {}).permit(permitted_params)
  end

end