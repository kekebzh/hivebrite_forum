class RepliesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_reply, only: [:edit, :update, :destroy, :show, :like, :dislike]
  before_action :set_post, only: [:create, :edit, :show, :update, :destroy, :like, :dislike]

  def new
  end

  def create
    @reply = @post.replies.create(params[:reply].permit(:content, :post_id, :user_id))
    @reply.user_id = current_user.id

    if @reply.save
      redirect_to post_path(@post), notice: "Reply successfully created"
    else
      redirect_to post_path(@post), notice: "Reply did not save. Please try again."
    end

  end

  def edit
  end

  def update
    @reply = @post.replies.find(params[:id])

    if current_user == @reply.user
      if @reply.update(reply_params)
        redirect_to post_path(@post), notice: "Reply successfully updated."
      else
        render :edit
      end
    else
      redirect_to post_path(@post), alert: "Your are not the reply author."
    end
  end

  def show
  end

  def destroy
    @reply = @post.replies.find(params[:id])

    if current_user == @reply.user
      @reply.destroy
      redirect_to post_path(@post), notice: "Reply successfully destroyed."
    else
      redirect_to post_path(@post), alert: "Your are not the reply author."
    end
  end

  def like
    @reply = Reply.find(params[:reply_id])
    @reply.upvote_by current_user
    redirect_to post_path(@post), notice: "Upvote !"
  end

  def dislike
    @reply = Reply.find(params[:reply_id])
    @reply.downvote_by current_user
    redirect_to post_path(@post), notice: "Downvote !"
  end

  private

  def set_post
    @post = Post.find(params[:post_id]) if params.key?(:post_id)
  end

  def set_reply
    @reply = Reply.find(params[:id]) if params.key?(:id)
  end

  def permitted_params
    [
      :content,
    ]
  end

  def reply_params
    params.fetch(:reply, {}).permit(permitted_params)
  end
end