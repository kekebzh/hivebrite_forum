class PostsController < ApplicationController
  before_action :set_post, only: [:edit, :show, :update, :destroy, :like, :dislike]
  before_action :find_channels, only: [:index, :show, :new, :edit]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @posts = Post.all.preload(:user, :channel).order('created_at desc').page(params[:page])
  end

  def show
    @posts = Post.all.preload(:user, :channel).order('created_at desc').page(params[:page])
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)

    if @post.save
      redirect_to post_path(@post), notice: "Post successfully created."
    else
      render :new
    end
  end

  def edit
  end

  def update
    if current_user == @post.user
      if @post.update(post_params)
        redirect_to post_path(@post), notice: "Post successfully updated."
      else
        render :edit
      end
    else
      redirect_to post_path(@post), alert: "Your are not the post author."
    end
  end

  def destroy
    if current_user == @post.user
      @post.destroy
      redirect_to root_path, notice: "Post successfully destroyed."
    else
      redirect_to post_path(@post), alert: "Your are not the post author."
    end
  end

  def like
    @post.upvote_by current_user
    redirect_to post_path(@post), notice: "Upvote !"
  end

  def dislike
    @post.downvote_by current_user
    redirect_to post_path(@post), notice: "Downvote !"
  end

  private

  def set_post
    @post = Post.find(params[:id]) if params.key?(:id)
  end

  def permitted_params
    [
        :title,
        :content,
        :channel_id,
    ]
  end

  def post_params
    params.fetch(:post, {}).permit(permitted_params)
  end

  def find_channels
    @channels = Channel.all.order('created_at desc')
  end

end