module ApplicationHelper

  def icon(id, style = "fa")
    id = id.to_s.tr("_", "-")
    content_tag(:span, class: "#{style} fa-#{id}") {}
  end

  def lf2br(str)
    str.to_s.gsub("\r", "").gsub("\n", "<br />").html_safe
  end

end
