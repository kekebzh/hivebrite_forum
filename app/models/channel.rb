class Channel < ApplicationRecord
  has_many :posts
  has_many :users, through: :posts

  validates :title, presence: true
end
