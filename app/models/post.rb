class Post < ApplicationRecord
  include Searchable
  acts_as_votable
  belongs_to :user
  belongs_to :channel
  has_many :replies, dependent: :destroy

  validates :title, presence: true
  validates :content, presence: true

end
