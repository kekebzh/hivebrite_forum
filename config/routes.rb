Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }
  root "posts#index"

  resources :channels
  resources :posts do
    member do
      post 'like', to: 'posts#like'
      post 'dislike', to: 'posts#dislike'
    end
    resources :replies do
      post 'like', to: 'replies#like'
      post 'dislike', to: 'replies#dislike'
    end
  end
end
