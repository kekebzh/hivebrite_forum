FactoryBot.define do
  factory :channel do
    title { Faker::Movie.title }
  end
end