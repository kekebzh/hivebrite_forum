FactoryBot.define do
  factory :reply do
    content { Faker::Lorem.paragraph_by_chars(number: 500, supplemental: true) }
    association :post
    association :user
  end
end