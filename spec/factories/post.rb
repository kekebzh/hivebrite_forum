FactoryBot.define do
  factory :post do
    title { Faker::Lorem.sentence }
    content { Faker::Lorem.paragraph_by_chars(number: 500, supplemental: true) }
    association :channel
    association :user
  end
end
