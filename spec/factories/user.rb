FactoryBot.define do
  factory :user do
    firstname                { Faker::Name.first_name}
    lastname                 { Faker::Name.last_name }
    email                    { "#{firstname}.#{lastname}@test.com" }
    password                 { "password"}
    password_confirmation    { "password" }
  end
end