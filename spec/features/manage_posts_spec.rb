require "rails_helper"

RSpec.feature "Manage Posts", :type => :feature do

  let!(:channel) { FactoryBot.create(:channel) }
  let!(:user)    { FactoryBot.create(:user) }
  let!(:post)    { FactoryBot.create(:post) }

  scenario "Authenticated user can create post" do
    login_as(user)

    visit '/posts/new'

    fill_in   'Title',          with: "Title post"
    fill_in   'Content',        with: "Content post"
    select    channel.title,    from: "Channel"

    click_button 'Create Post'
    expect(page).to have_content 'Post successfully created'
  end

  scenario "Unauthenticated user can't create post" do
    visit "/"
    click_link 'New Post'
    expect(page).to have_content 'You need to sign in or sign up before continuing'
  end

  scenario "Authenticated user can edit his post" do
    login_as(user)

    post = Post.create(
        :title => "Test title",
        :content => "Test Content",
        :user => user,
        :channel => channel
    )

    visit "/posts/#{post.id}/edit"
    expect(page).to have_content("#{post.content}")

    fill_in   'Title',          with: "Title post"
    fill_in   'Content',        with: "Content post"
    select    channel.title,    from: "Channel"

    click_button 'Update Post'
    expect(page).to have_content 'Post successfully updated'
  end

  scenario "Unauthenticated user can't edit post" do
    visit "/posts/#{post.id}"
    click_link 'Edit Post'
    expect(page).to have_content 'You need to sign in or sign up before continuing'
  end

  scenario "Authenticated user can destroy his post" do
    login_as(user)

    post = Post.create(
        :title => "Test title",
        :content => "Test Content",
        :user => user,
        :channel => channel
    )

    visit "/posts/#{post.id}"
    expect(page).to have_content("#{post.content}")

    click_link 'Delete'
    expect(page).to have_content 'Post successfully destroyed'
  end

  scenario "Unauthenticated user can't destroy post" do
    visit "/posts/#{post.id}"
    click_link 'Delete'
    expect(page).to have_content 'You need to sign in or sign up before continuing'
  end

end