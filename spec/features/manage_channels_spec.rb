require "rails_helper"

RSpec.feature "Manage Channels", :type => :feature do

  let!(:user)    { FactoryBot.create(:user) }

  scenario "Authenticated user can create channel" do
    login_as(user)

    visit '/channels/new'
    fill_in 'Title', with: "Title content"
    click_button 'Create Channel'
    expect(page).to have_content 'Channel successfully created'
  end

  scenario "Unauthenticated user can't create channel" do
    visit "/"
    click_link 'New Channel'
    expect(page).to have_content 'You need to sign in or sign up before continuing'
  end

end