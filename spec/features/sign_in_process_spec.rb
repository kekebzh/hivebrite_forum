require "rails_helper"

RSpec.feature "Sign In", :type => :feature do

  let!(:user) {create(:user)}

  scenario "Signing in with correct credentials" do
    visit '/users/sign_in'

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password

    click_button 'Log in'
    expect(page).to have_content 'Signed in successfully'
  end

  scenario "Signing in with wrong credentials" do
    visit '/users/sign_in'

    fill_in 'Email', with: user.email
    fill_in 'Password', with: "toto"

    click_button 'Log in'
    expect(page).to have_content 'Invalid Email or password'
  end
end