require "rails_helper"

RSpec.feature "Sign Up", :type => :feature do

  scenario "Signing up user with valid attributes" do
    visit '/users/sign_up'

    fill_in 'Firstname', with: "mich"
    fill_in 'Lastname', with: "prig"
    fill_in 'Email', with: "user@test.com"
    fill_in 'Password', with: "password"
    fill_in 'Password confirmation', with: "password"

    click_button 'Sign up'
    expect(page).to have_content 'You have signed up successfully'
  end

  scenario "Signing up user with non valid attributes" do
    visit '/users/sign_up'

    fill_in 'Firstname', with: ""
    fill_in 'Lastname', with: ""
    fill_in 'Email', with: "user@test.com"
    fill_in 'Password', with: "password"
    fill_in 'Password confirmation', with: "password"

    click_button 'Sign up'
    expect(page).to have_content 'errors prohibited this user from being saved'
  end

end