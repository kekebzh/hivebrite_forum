require "rails_helper"

RSpec.feature "Manage Vote", :type => :feature do

  let!(:channel) { FactoryBot.create(:channel) }
  let!(:user)    { FactoryBot.create(:user) }
  let!(:post)    { FactoryBot.create(:post) }

  scenario "Authenticated user like post" do
    login_as(user)

    visit "/posts/#{post.id}"

    click_link 'Like Post'
    expect(page).to have_content 'Upvote'
  end

  scenario "Authenticated user dislike post" do
    login_as(user)

    visit "/posts/#{post.id}"

    click_link 'Dislike Post'
    expect(page).to have_content 'Downvote'
  end

  scenario "Authenticated user like reply" do
    login_as(user)

    reply = Reply.create(
        post: post,
        user: user,
        content: "content reply"
    )

    visit "/posts/#{post.id}"

    click_link 'Like Reply'
    expect(page).to have_content 'Upvote'
  end

  scenario "Authenticated user dislike reply" do
    login_as(user)

    reply = Reply.create(
        post: post,
        user: user,
        content: "content reply"
    )

    visit "/posts/#{post.id}"

    click_link 'Dislike Reply'
    expect(page).to have_content 'Downvote'
  end
end