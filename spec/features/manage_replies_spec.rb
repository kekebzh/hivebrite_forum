require "rails_helper"

RSpec.feature "Manage Reply", :type => :feature do

  let!(:channel) { FactoryBot.create(:channel) }
  let!(:user)    { FactoryBot.create(:user) }
  let!(:post)    { FactoryBot.create(:post) }

  scenario "Authenticated user can reply post" do
    login_as(user)

    visit "/posts/#{post.id}"

    fill_in 'Content', with: "Content reply"
    click_button 'Leave a reply'
    expect(page).to have_content 'Reply successfully created'
  end
end