require 'rails_helper'

RSpec.describe Channel, type: :model do

  describe 'validations' do
    it { should validate_presence_of(:title) }
  end

  it "should have a valid factory" do
    channel = build(:channel)
    expect(channel).to be_valid
  end
end