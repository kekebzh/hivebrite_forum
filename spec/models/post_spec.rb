require 'rails_helper'

RSpec.describe Post, type: :model do

  describe "Associations" do
    it { should belong_to(:user).class_name('User') }
    it { should belong_to(:channel).class_name('Channel') }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:content) }
  end

  it "should have a valid factory" do
    post = build(:post)
    expect(post).to be_valid
  end
end
