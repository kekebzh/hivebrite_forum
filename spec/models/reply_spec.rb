require 'rails_helper'

RSpec.describe Reply, type: :model do

  describe "Associations" do
    it { should belong_to(:user).class_name('User') }
    it { should belong_to(:post).class_name('Post') }
  end

  describe 'validations' do
    it { should validate_presence_of(:content) }
  end

  it "should have a valid factory" do
    reply = build(:reply)
    expect(reply).to be_valid
  end
end